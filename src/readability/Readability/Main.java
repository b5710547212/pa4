package readability.Readability;

import java.net.MalformedURLException;
import java.net.URL;

import readability.ReadabilityGUI;
import readability.WordCounter;

public class Main {

	public static void main(String[] args) {
		if(args.length > 0) {
			URL url;
			try {
				url = new URL( args[0] );
				WordCounter counter = new WordCounter();
				int wordcount = counter.countWords( url );
				int syllables = counter.getSyllableCount( );
				double flesch = 206.835 - 84.6*((syllables*1.0) / (wordcount*1.0))- 1.015*((wordcount*1.0) / (counter.getSentence()*1.0) );
				String readability;
				if(flesch>100)
					readability = "4th grade student (elementary school)" ;
				else if(flesch>90)
					readability = "5th grade student";
				else if(flesch>80)
					readability = "6th grade student";
				else if(flesch>70)
					readability = "7th grade student";
				else if(flesch>65)
					readability = "8th grade student";
				else if(flesch>60)
					readability = "9th grade student";
				else if(flesch>50)
					readability = "High school student";
				else if(flesch>30)
					readability = "College student";
				else if(flesch>0)
					readability = "College graduate";
				else
					readability = "Advanced degree graduate";
				System.out.printf("Filename:\t\t%s\nNumber of Syllables:\t\t%d\nNumber of Words:\t\t%d\nNumber of Sentences:\t\t%d\nFlesch Index:\t\t%.1f\nReadability:\t\t%s"
						, args[0],syllables,wordcount,counter.getSentence(),flesch,readability );
			} catch (MalformedURLException e) {
			}
		}
		else {
			ReadabilityGUI rg = new ReadabilityGUI();
			rg.run();
		}
	}
}

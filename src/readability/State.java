package readability;


public interface State {
	public void enterState(char c);
}

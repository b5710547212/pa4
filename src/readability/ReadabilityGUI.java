package readability;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class ReadabilityGUI extends JFrame{

	private JPanel mainPanel;
	private JPanel headPanel;
	private JLabel fileURLLabel;
	private JTextField inputField;
	private JButton browseBtn;
	private JButton countBtn;
	private JButton clearBtn;
	private JTextArea displayArea;

	public ReadabilityGUI() {

		initComponents();
		this.setTitle("File Counter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.pack();

	}

	public void initComponents(){

		mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.Y_AXIS ) );

		headPanel = new JPanel();

		fileURLLabel = new JLabel("File or URL name: ");
		inputField = new JTextField(10);
		browseBtn = new JButton("Browse...");
		countBtn = new JButton("Count");
		clearBtn = new JButton("Clear");
		displayArea = new JTextArea(6,30);

		headPanel.add(fileURLLabel);

		headPanel.add(inputField);

		headPanel.add(browseBtn);
		headPanel.add(countBtn);
		headPanel.add(clearBtn);


		browseBtn.addActionListener(new browseBtn());
		countBtn.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					URL url = new URL( inputField.getText() );
					WordCounter counter = new WordCounter();
					int wordcount = counter.countWords( url );
					int syllables = counter.getSyllableCount( );
					double flesch = 206.835 - 84.6*((syllables*1.0) / (wordcount*1.0))- 1.015*((wordcount*1.0) / (counter.getSentence()*1.0) );
					String readability;
					if(flesch>100)
						readability = "4th grade student (elementary school)" ;
					else if(flesch>90)
						readability = "5th grade student";
					else if(flesch>80)
						readability = "6th grade student";
					else if(flesch>70)
						readability = "7th grade student";
					else if(flesch>65)
						readability = "8th grade student";
					else if(flesch>60)
						readability = "9th grade student";
					else if(flesch>50)
						readability = "High school student";
					else if(flesch>30)
						readability = "College student";
					else if(flesch>0)
						readability = "College graduate";
					else
						readability = "Advanced degree graduate";
					displayArea.setText(String.format("Filename:\t\t%s\nNumber of Syllables:\t\t%d\nNumber of Words:\t\t%d\nNumber of Sentences:\t\t%d\nFlesch Index:\t\t%.1f\nReadability:\t\t%s"
							, inputField.getText(),syllables,wordcount,counter.getSentence(),flesch,readability ) );
				} catch (MalformedURLException e1) {
				}
			}
		});
		clearBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displayArea.setText("");
			}		
		});

		mainPanel.add(headPanel);
		mainPanel.add(displayArea);

		super.add(mainPanel);

	}

	public void run() {

		this.setVisible(true);

	}

	class browseBtn implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			fc.showOpenDialog(mainPanel);
			inputField.setText("file:" + fc.getSelectedFile().getPath());
		}
	}

	public static void main(String[] args) {

		ReadabilityGUI rg = new ReadabilityGUI();
		rg.run();

	}

}

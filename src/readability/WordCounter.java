package readability;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * count word and syllable.
 * @author Woramate Jumroonsilp
 *
 */
public class WordCounter {

	private State state;
	public int syllable, totalSyllable, sentence = 0;
	
	State STARTSTATE = new State(){
		@Override
		public void enterState(char c) {
			if(isEStart(c)) setState(E_FIRST);
			else if(isVowel(c)) setState(VOWELSTATE);
			else setState(CONSONANTSTATE);
		}
	};
	State VOWELSTATE = new State(){
		@Override
		public void enterState(char c) {
			if(isVowel(c)) setState(VOWELSTATE);
			else {
				setState(CONSONANTSTATE);
				syllable++;
			}
		}
	};
	State CONSONANTSTATE = new State(){
		@Override
		public void enterState(char c) {
			if(isEStart(c)) setState(E_FIRST);
			else if(isVowel(c)||c=='y') setState(VOWELSTATE);
			else setState(CONSONANTSTATE);
		}
	};
	State E_FIRST = new State(){
		@Override
		public void enterState(char c) {
			if(isVowel(c)) setState(VOWELSTATE);
			else{
				setState(CONSONANTSTATE);
				if(c!='-')syllable++;
			}
		}
	};
	State NONWORD = new State(){
		@Override
		public void enterState(char c) {
			syllable = 0;
		}
	};
	State SENTENCES = new State(){
		@Override
		public void enterState(char c) {
			
		}
	};
	
	/**
	 * count syllable of word
	 * @param input
	 * @return
	 */
	public int countSyllables( String input ) {
		this.state = STARTSTATE;
		this.syllable = 0;
		input = input.toLowerCase();
		for(int i=0 ; i<input.length() ; i++)
		{
			char temp = input.charAt(i);
			if(!isLetter(temp) && temp!='-' && temp!='\''){
				setState(NONWORD);
				break;
			}
			this.state.enterState(temp);
		}
		if(this.state==VOWELSTATE) syllable++;
		if(this.state==E_FIRST && syllable==0) syllable++;
		if(input.charAt(0) == '-' || input.charAt(input.length()-1)=='-')syllable=0;
		if(isEndSentence(input.charAt(input.length()-1))) sentence++;
		return this.syllable;
	}
	
	/**
	 * count word from input stream
	 * @param instream
	 * @return number of word
	 */
	public int countWords(InputStream instream)
	{
		int product = 0;
		Scanner scanner = new Scanner(instream);
		while(scanner.hasNext()){
			product++;
			this.totalSyllable+=countSyllables(scanner.next());
		}
		return product;
	}
	
	/**
	 * count word from url
	 * @param url
	 * @return number of word
	 */
	public int countWords(URL url)
	{
		try {
			InputStream in = url.openStream( );
			return countWords(in);
		} catch (IOException e) {
			return 0;
		}
	}
	
	/**
	 * get number of all syllable
	 * @return
	 */
	public int getSyllableCount()
	{
		return totalSyllable;
	}
	
	/**
	 * get number of sentence
	 * @return
	 */
	public int getSentence() {
		return this.sentence;
	}
	
	/**
	 * set state of program
	 * @param newState
	 */
	public void setState( State newState ) {
		this.state = newState;
	}
	
	/**
	 * check if this is vowel
	 * @param c
	 * @return
	 */
	public boolean isVowel(char c){
		return "aeiou".indexOf(c) >= 0;
	}
	
	/**
	 * check for end sentence
	 * @param c
	 * @return
	 */
	public boolean isEndSentence(char c) {
		return c=='.'||c==';'||c=='?'||c=='!';
	}
	
	/**
	 * check if this is E_Start
	 * @param c
	 * @return
	 */
	public boolean isEStart( char c ){
		return c=='E'||c=='e';
	}
	
	/**
	 * check if this character is a letter
	 * @param c
	 * @return
	 */
	public boolean isLetter( char c ){
		return Character.isLetter(c);
	}
	
	public static void main(String[] args) throws MalformedURLException{
		final String DICT_URL = "http://www.cpe.ku.ac.th/~jim/219141/readability/simple.txt";
		URL url = new URL( DICT_URL );
		WordCounter counter = new WordCounter();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		System.out.println("Reading words from:"+DICT_URL);
		System.out.println("Counted " + syllables + " syllables in " + wordcount + " words " + counter.getSentence() + " sentences" );
	}
	
}

